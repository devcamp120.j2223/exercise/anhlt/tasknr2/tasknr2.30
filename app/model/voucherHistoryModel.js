// khai báo mongoose
const mongoose = require('mongoose');

// khởi tạo schema
const schema = mongoose.Schema;

const PrizeHistorySchema = new schema({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: [
        {
            type: mongoose.Types.ObjectId,
            ref: "user",
            required: true
        }
    ],
    voucher: [
        {
            type: mongoose.Types.ObjectId,
            ref: "voucher",
            required: true
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

//export thành một modules
module.exports = mongoose.model("prizehistory", PrizeHistorySchema)