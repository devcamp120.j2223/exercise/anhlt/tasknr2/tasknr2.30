// Khai báo thư viện mongoose
const mongoose = require('mongoose');

//  Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//  Khởi tạo Schema với các thuộc tính được yêu cầu
const diceHistorySchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    user: [
        {
            type: mongoose.Types.ObjectId,
            ref: "user",
            required: true,
        }
    ],
    dice: {
        type: Number,
        required: true,
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

//  EXPORT
module.exports = mongoose.model("dice-history", diceHistorySchema);